package com.example.demo.exception;

public class ToDoZonedDateTimeException extends Exception {
    public ToDoZonedDateTimeException() {
        super("ZonedDateTime is wrongly specified");
    }
}
