package com.example.demo.controller;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.exception.ToDoZonedDateTimeException;
import com.example.demo.service.ToDoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ToDoController {

    private final ToDoService toDoService;

    public ToDoController(ToDoService toDoService) {
        this.toDoService = toDoService;
    }

    @ExceptionHandler({ToDoNotFoundException.class})
    public String handleException(Exception ex) {
        return ex.getMessage();
    }

    @ExceptionHandler({ToDoZonedDateTimeException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleTimeException(ToDoZonedDateTimeException ex) {
        return ex.getMessage();
    }

    @GetMapping("/todos")
    @Valid List<ToDoResponse> getAll() {
        return toDoService.getAll();
    }

    @GetMapping("/todos/before/{time}")
    @Valid List<ToDoResponse> getAllBefore(@PathVariable String time) throws ToDoZonedDateTimeException {
        return toDoService.getAllCompletedBefore(time);
    }

    @GetMapping("/todos/completed")
    @Valid List<ToDoResponse> getAllCompleted() {
        return toDoService.getAllCompleted();
    }

    @PostMapping("/todos")
    @Valid ToDoResponse save(@Valid @RequestBody ToDoSaveRequest todoSaveRequest) throws ToDoNotFoundException {
        return toDoService.upsert(todoSaveRequest);
    }

    @PutMapping("/todos/{id}/complete")
    @Valid ToDoResponse save(@PathVariable Long id) throws ToDoNotFoundException {
        return toDoService.completeToDo(id);
    }

    @GetMapping("/todos/{id}")
    @Valid ToDoResponse getOne(@PathVariable Long id) throws ToDoNotFoundException {
        return toDoService.getOne(id);
    }

    @DeleteMapping("/todos/{id}")
    void delete(@PathVariable Long id) {
        toDoService.deleteOne(id);
    }

}